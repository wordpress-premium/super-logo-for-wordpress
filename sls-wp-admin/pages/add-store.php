<?php
if (!empty($_GET['pg']) && isset($wpdb) && $_GET['pg']=='add-store') { include_once(SLS_WP_INCLUDES_PATH."/top-nav.php"); print "<script>jQuery('.locations').removeAttr('id');</script>"; } 

if (!isset($wpdb)){ include("../../../../wp-load.php"); }
if (!defined("SLS_WP_INCLUDES_PATH")) { include("../sls-wp-define.php"); }
if (!function_exists("sls_wp_initialize_variables")) { include("../sls-wp-functions.php"); }
if (defined('SLS_WP_ADDONS_PLATFORM_FILE') && file_exists(SLS_WP_ADDONS_PLATFORM_FILE)) { include_once(SLS_WP_ADDONS_PLATFORM_FILE); } //check if this inclusion is actually necessary here anymore - 3/19/14

print "<div class='wrap'>";
/*print "<h2>".__("Add Locations", SLS_WP_TEXT_DOMAIN)."</h2><br>";*/

global $wpdb;
sls_wp_initialize_variables();

//Inserting addresses by manual input
if (!empty($_POST['sls_wp_store']) && (empty($_GET['mode']) || $_GET['mode']!="pca")) {
	if (!empty($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], "add-location_single")){
		sls_wp_add_location();
		print "<div class='sls_wp_admin_success'>".__(" Store succeslsully added",SLS_WP_TEXT_DOMAIN).". $view_link</div> <!--meta http-equiv='refresh' content='0'-->"; 
	} else {
		print "<div class='sls-wp-menu-alert'>".__(" Store failed to be added to the database.",SLS_WP_TEXT_DOMAIN).". $view_link</div>"; 
	}
}

//Importing addresses from an local or remote database
if (!empty($_POST['remote']) && trim($_POST['query'])!="" || !empty($_POST['finish_import'])) {
	
	if (!empty($_POST['server']) && preg_match("@.*\..{2,}@", $_POST['server'])) {
		include(SLS_WP_ADDONS_PATH."/db-importer/remoteConnect.php");
	} else {
		if (file_exists(SLS_WP_ADDONS_PATH."/db-importer/localImport.php")) {
			include(SLS_WP_ADDONS_PATH."/db-importer/localImport.php");
		} elseif (file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/localImport.php")) {
			include(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/csv-xml-importer-exporter.php");
			include(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/localImport.php");
		}
	}
	//for intermediate step match column data to field headers
	if (empty($_POST['finish_import']) || $_POST['finish_import']!="1") {exit();}
}

//Importing CSV file of addresses
$newfile="temp-file.csv"; 
//$root=plugin_dir_path(__FILE__); //dirname(plugin_basename(__FILE__)); die($root);
$root=SLS_WP_ADDONS_PATH;
$target_path="$root/";
//print_r($_FILES);
if (!empty($_FILES['csv_import']['tmp_name']) && move_uploaded_file($_FILES['csv_import']['tmp_name'], "$root/$newfile") && file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/csvImport.php")) {
	include(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/csvImport.php");
}
else{
		//echo "<div style='background-color: rgb(255, 124, 86); padding:5px'>There was an error uploading the file, please try again. </div>";
}

//If adding via the Point, Click, Add map (accepting AJAX)
if (!empty($_REQUEST['mode']) && $_REQUEST['mode']=="pca") {
	include(SLS_WP_ADDONS_PATH."/point-click-add/pcaImport.php");
}

print sls_wp_location_form("add");

function csv_importer(){
	global $sls_wp_uploads_path, $sls_wp_path, $text_domain, $web_domain;
	if (file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/csv-import-form.php")) {
		include(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/csv-import-form.php");
		print "<br>";
	}
}
function db_importer(){
	global $sls_wp_uploads_path, $sls_wp_path, $text_domain, $web_domain;
	if (file_exists(SLS_WP_ADDONS_PATH."/db-importer/db-import-form.php")) {
		//include(SLS_WP_INCLUDES_PATH."/sls-wp-env.php");
		include(SLS_WP_ADDONS_PATH."/db-importer/db-import-form.php");
	}
}
function point_click_add(){
	global $sls_wp_uploads_path, $sls_wp_path, $text_domain, $web_domain;
	if (file_exists(SLS_WP_ADDONS_PATH."/point-click-add/point-click-add-form.php")) {
		include(SLS_WP_ADDONS_PATH."/point-click-add/point-click-add-form.php");
	}
}
function sls_wp_csv_db_pca_forms(){
  if (file_exists(SLS_WP_ADDONS_PATH."/db-importer/db-import-form.php") || file_exists(SLS_WP_ADDONS_PATH."/point-click-add/point-click-add-form.php") || file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/csv-import-form.php")) {	
	print "<table><tr>";
	if (file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/csv-import-form.php") || file_exists(SLS_WP_ADDONS_PATH."/db-importer/db-import-form.php")) {
		print "<td style='vertical-align:top; padding-top:0px'>";
		csv_importer();
		db_importer();
		print "</td>";
	}
	if (file_exists(SLS_WP_ADDONS_PATH."/point-click-add/point-click-add-form.php")) {
		print "<td style='vertical-align:top; padding-top:0px'>";
		point_click_add();
		print "</td>";
	}	
		print "</tr></table>";
  }
}
if (function_exists("addto_sls_wp_hook")) {
	addto_sls_wp_hook('sls_wp_add_location_forms', 'csv_importer','','','csv-xml-importer-exporter');
	addto_sls_wp_hook('sls_wp_add_location_forms', 'db_importer','','','db-importer');
	addto_sls_wp_hook('sls_wp_add_location_forms', 'point_click_add','','','point-click-add');
} else {
	sls_wp_csv_db_pca_forms();
}

if (function_exists("do_sls_wp_hook")) {do_sls_wp_hook('sls_wp_add_location_forms', 'select-top');}



print "
</div>";

include(SLS_WP_INCLUDES_PATH."/sls-wp-footer.php");
?>