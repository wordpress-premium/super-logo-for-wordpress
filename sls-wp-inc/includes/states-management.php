<?php

print "<table width='100%' cellpadding='5px' cellspacing='0' style='border:solid silver 1px' id='mgmt_bar' class='widefat'>
<thead><tr>
<th style='/*background-color:#000;*/ width:10%; vertical-align:middle; font-family:inherit; font-size:12px;'><input class='button-primary' type='button' value='".__("Delete", SLS_WP_TEXT_DOMAIN)."' onclick=\"if(confirm('".__("Are you sure you want to remove the State[s)", SLS_WP_TEXT_DOMAIN)."?')){LF=document.forms['locationForm'];LF.act.value='delete';LF.submit();}else{return false;}\"></th>";
$extra=(!empty($extra))? $extra : "" ;

if (function_exists("addto_sls_wp_hook")) {addto_sls_wp_hook('sls_wp_mgmt_bar_links', 'export_links', '', '', 'csv-xml-importer-exporter');} 
$mgmt_bgcolor=((!function_exists("addto_sls_wp_hook") && file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/export-links.php")) || (!empty($sls_wp_hooks['sls_wp_mgmt_bar_links'])) )? "/*background-color:#e6e6e6; background-image:none;*/ border-left: solid #ccc 1px; border-right: solid #ccc 1px;" : "";
print "<th style='width:30%; text-align:center; color:black; font-family:inherit; font-size:12px; {$mgmt_bgcolor} /**/' class='youhave'>";
function export_links() {
		global $sls_wp_uploads_path, $web_domain, $extra, $sls_wp_base, $sls_wp_uploads_base, $text_domain;
		if (file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/export-links.php")) {
			$sls_wp_real_base=$sls_wp_base; $sls_wp_base=$sls_wp_uploads_base;
			include(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/export-links.php");
			$sls_wp_base=$sls_wp_real_base;
		}
		
} 
if (!function_exists("addto_sls_wp_hook")) {export_links();}
if (function_exists("do_sls_wp_hook")) { do_sls_wp_hook('sls_wp_mgmt_bar_links', 'select-right');  }

print "</th>";
print "<th style='/*background-color:#000;*/ width:50%; text-align:right; /*color:white;*/ vertical-align:middle; font-family:inherit; font-size:12px;'>";

  function multi_updater() {
	global $sls_wp_uploads_path, $text_domain, $web_domain;
	if (file_exists(SLS_WP_ADDONS_PATH."/multiple-field-updater/multiple-field-update-form.php") && (sls_wp_data('sls_wp_location_updater_type')=="Multiple Fields" || function_exists("do_sls_wp_hook"))) {
		include(SLS_WP_ADDONS_PATH."/multiple-field-updater/multiple-field-update-form.php");
	}
  }
	if (function_exists("addto_sls_wp_hook")) {
	    if (is_dir(SLS_WP_ADDONS_PATH."/multiple-field-updater/")){
		addto_sls_wp_hook('sls_wp_mgmt_bar_form', 'multi_updater', '', '', 'multiple-field-updater');
	    }
	} elseif (!function_exists("addto_sls_wp_hook")) {
		if (file_exists(SLS_WP_ADDONS_PATH."/multiple-field-updater/multiple-field-update-form.php") && sls_wp_data('sls_wp_location_updater_type')=="Multiple Fields") {
			multi_updater();
		} else {
		}
	}

	if (function_exists("do_sls_wp_hook")) {do_sls_wp_hook('sls_wp_mgmt_bar_form', 'select');
  }
print "</th></tr></thead></table>
";

?>