<?php
if (is_dir(SLS_WP_THEMES_PATH)) {
	$theme_dir=opendir(SLS_WP_THEMES_PATH); 
	$theme_str="";
	
	while (false !== ($a_theme=readdir($theme_dir))) {
		if (!preg_match("@^\.{1,2}.*$@", $a_theme) && !preg_match("@\.(php|txt|htm(l)?)@", $a_theme)) {

			$selected=($a_theme==$sls_wp_vars['theme'])? " selected " : "";
			$theme_str.="<option value='$a_theme' $selected>$a_theme</option>\n";
		}
	}
}

$zl_arr=array();
for ($i=0; $i<=19; $i++) {
	$zl_arr[]=$i;
}


$toggle_settings["".__("Yes", SLS_WP_TEXT_DOMAIN).""]="1";
$toggle_settings["".__("No", SLS_WP_TEXT_DOMAIN).""]="0";

$toggle_settings_options="";
foreach($toggle_settings as $key=>$value) {
	$selected=($sls_wp_vars['sls_wp_map_settings']==$value)? " selected " : "";
	$toggle_settings_options.="<option value='$value' $selected>$key</option>\n";
}

$toggle_settings2["".__("Yes", SLS_WP_TEXT_DOMAIN).""]="1";
$toggle_settings2["".__("No", SLS_WP_TEXT_DOMAIN).""]="0";

$toggle_settings_options2="";

foreach($toggle_settings2 as $key=>$value) {
	$selected2=($sls_wp_vars['region_show']==$value)? " selected " : "";
	$toggle_settings_options2.="<option value='$value' $selected2>$key</option>\n";
}


$toggle_settings3["".__("Yes", SLS_WP_TEXT_DOMAIN).""]="1";
$toggle_settings3["".__("No", SLS_WP_TEXT_DOMAIN).""]="0";

$toggle_settings_options3="";

foreach($toggle_settings3 as $key=>$value) {
	$selected3=($sls_wp_vars['title_show']==$value)? " selected " : "";
	$toggle_settings_options3.="<option value='$value' $selected3>$key</option>\n";
}

$toggle_settings4["".__("Yes", SLS_WP_TEXT_DOMAIN).""]="1";
$toggle_settings4["".__("No", SLS_WP_TEXT_DOMAIN).""]="0";

$toggle_settings_options4="";

foreach($toggle_settings4 as $key=>$value) {
	$selected4=($sls_wp_vars['to_top']==$value)? " selected " : "";
	$toggle_settings_options4.="<option value='$value' $selected4>$key</option>\n";
}

$user_role_settings["".__("Administrator", SLS_WP_TEXT_DOMAIN).""]="administrator";
$user_role_settings["".__("Author", SLS_WP_TEXT_DOMAIN).""]="author";
$user_role_settings["".__("Subscriber", SLS_WP_TEXT_DOMAIN).""]="subscriber";
$user_role_settings["".__("Contributor", SLS_WP_TEXT_DOMAIN).""]="contributor";
$user_role_settings["".__("Editor", SLS_WP_TEXT_DOMAIN).""]="editor";

if (function_exists("get_sls_current_user_role")){
$sls_role=get_sls_current_user_role();
}else{
$sls_role='administrator';
}
if($sls_role=='administrator'){
$user_role_set='';
$ex_cat = explode(",", $sls_wp_vars['sls_user_role']);
$ex_cat = array_map( 'trim', $ex_cat );
foreach($user_role_settings as $key=>$value) {
	$selected2=(in_array($value,$ex_cat))? 'selected="selected"' : '';	
	$desable=($value=='administrator')? 'disabled' : '';
	$user_role_set.="<option value='$value' $selected2 $desable>$key</option>\n";
}


$layout_settings_options="";
$selected1=($sls_wp_vars['sls_layout']=='1')? " checked " : "";
$selected2=($sls_wp_vars['sls_layout']=='2')? " checked " : "";
$layout_settings_options.="<img class='ssf_layout_settings' src='".SLS_WP_BASE."/images/standard.png'><span class='sls_layout'><input name='sls_layout' type='radio' value='1' $selected1>&nbsp;Standard</span>\n";
$layout_settings_options.="<img class='ssf_layout_settings' src='".SLS_WP_BASE."/images/metro.png'><span class='sls_layout'><input name='sls_layout' type='radio' value='2' $selected2>&nbsp;Metro</span>\n";

$sls_wp_mdo[] = array("field_name" => "sls_layout", "default" => "1", "input_zone" => "defaults", "label" =>  __("Layout", SLS_WP_TEXT_DOMAIN), "input_template" => "$layout_settings_options");

$grid_settings["".__("3 (Default)", SLS_WP_TEXT_DOMAIN).""]="4";
$grid_settings["".__("4", SLS_WP_TEXT_DOMAIN).""]="3";
$grid_settings["".__("6", SLS_WP_TEXT_DOMAIN).""]="2";

$grid_settings_options="";

foreach($grid_settings as $key=>$value) {
	$selected=($sls_wp_vars['sls_grid']==$value)? " selected " : "";
	$grid_settings_options.="<option value='$value' $selected>$key</option>\n";
}

$sls_wp_mdo[] = array("field_name" => "sls_grid", "default" => "3", "input_zone" => "defaults", "label" =>  __("Column per Grid", SLS_WP_TEXT_DOMAIN), "input_template" => "<select name='sls_grid'>\n$grid_settings_options</select><small>(For Metro Layout Only)</small>");

$sls_wp_mdo[] = array("field_name" => "sls_user_role", "default" => "administrator", "input_zone" => "defaults", "label" =>  __("User Role", SLS_WP_TEXT_DOMAIN), "input_template" => "<select name='sls_user_role[]' class='chosen-select' multiple>\n$user_role_set</select>");
}

$sls_wp_mdo[] = array("field_name" => "sls_wp_map_settings", "default" => "geo", "input_zone" => "defaults", "label" => __("Show Number of Results", SLS_WP_TEXT_DOMAIN), "input_template" => "<select name='sls_wp_map_settings'>\n$toggle_settings_options</select>", "stripslashes" => 1);

$sls_wp_mdo[] = array("field_name" => "region_show", "default" => "1", "input_zone" => "defaults", "label" => __("Show Category", SLS_WP_TEXT_DOMAIN), "input_template" => "<select name='region_show'>\n$toggle_settings_options2</select>", "stripslashes" => 1);

$sls_wp_mdo[] = array("field_name" => "title_show", "default" => "1", "input_zone" => "defaults", "label" => __("Show Title", SLS_WP_TEXT_DOMAIN), "input_template" => "<select name='title_show'>\n$toggle_settings_options3</select>", "stripslashes" => 1);

$grayscale_settings["".__("No (Default)", SLS_WP_TEXT_DOMAIN).""]="false";
$grayscale_settings["".__("Yes", SLS_WP_TEXT_DOMAIN).""]="true";
$grayscale_settings["".__("Logo Only", SLS_WP_TEXT_DOMAIN).""]="logo";
$grayscale_settings["".__("Logo (without hover)", SLS_WP_TEXT_DOMAIN).""]="logo_w_h";

$grayscale_set_options="";
foreach($grayscale_settings as $key=>$value) {
	$selected=($sls_wp_vars['grayscale_setting']==$value)? " selected " : "";
	$grayscale_set_options.="<option value='$value' $selected>$key</option>\n";
}
$sls_wp_mdo[] = array("field_name" => "grayscale_setting", "default" => "false", "input_zone" => "defaults", "label" => __("Grayscale", SLS_WP_TEXT_DOMAIN), "input_template" => "<select name='grayscale_setting'>\n$grayscale_set_options</select>", "stripslashes" => 1);


$sls_wp_mdo[] = array("field_name" => "to_top", "default" => "1", "input_zone" => "defaults", "label" => __("To Top", SLS_WP_TEXT_DOMAIN), "input_template" => "<select name='to_top'>\n$toggle_settings_options4</select>", "stripslashes" => 1);


// styles



//map custom code	
$main_bg = '';
$brands_label  = '';
$sls_wp_vars['brands_label'];



$sls_wp_mdo[] = array("field_name" => "main_bg", "default" => "", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Main Background", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='main_bg' value=\"$sls_wp_vars[main_bg]\" class=\"my-color-field\" >");
		
$sls_wp_mdo[] = array("field_name" => "style_panel_bg", "default" => " ", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Panel Background", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='style_panel_bg' value=\"$sls_wp_vars[style_panel_bg]\" class=\"my-color-field\" size='14'> ");			
	

$sls_wp_mdo[] = array("field_name" => "style_panel_font", "default" => "", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Panel Font", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='style_panel_font' value=\"$sls_wp_vars[style_panel_font]\" class=\"my-color-field\" >");

$sls_wp_mdo[] = array("field_name" => "style_show_all_font", "default" => "", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Show All Font", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='style_show_all_font' value=\"$sls_wp_vars[style_show_all_font]\" class=\"my-color-field\" >");

$sls_wp_mdo[] = array("field_name" => "style_contact_button_bg", "default" => "", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Button Background", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='style_contact_button_bg' value=\"$sls_wp_vars[style_contact_button_bg]\" class=\"my-color-field\" >");

$sls_wp_mdo[] = array("field_name" => "style_contact_button_font", "default" => "", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Button Font", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='style_contact_button_font' value=\"$sls_wp_vars[style_contact_button_font]\" class=\"my-color-field\" >");



$sls_wp_mdo[] = array("field_name" => "style_results_bg", "default" => "", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Results Background", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='style_results_bg' value=\"$sls_wp_vars[style_results_bg]\" class=\"my-color-field\" >");

$sls_wp_mdo[] = array("field_name" => "style_results_hl_bg", "default" => "", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Results Highlighted Background", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='style_results_hl_bg' value=\"$sls_wp_vars[style_results_hl_bg]\" class=\"my-color-field\" >");

$sls_wp_mdo[] = array("field_name" => "style_results_hover_bg", "default" => "", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Results Hover Background", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='style_results_hover_bg' value=\"$sls_wp_vars[style_results_hover_bg]\" class=\"my-color-field\" >");

$sls_wp_mdo[] = array("field_name" => "category_btn_bg", "default" => "", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Category Button Background", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='category_btn_bg' value=\"$sls_wp_vars[category_btn_bg]\" class=\"my-color-field\" ><small>(For Metro Layout Only)</small>");

$sls_wp_mdo[] = array("field_name" => "category_btn_hover", "default" => "", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Category Active Background", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='category_btn_hover' value=\"$sls_wp_vars[category_btn_hover]\" class=\"my-color-field\" ><small>(For Metro Layout Only)</small>");

$sls_wp_mdo[] = array("field_name" => "category_font_color", "default" => "", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Category Font", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='category_font_color' value=\"$sls_wp_vars[category_font_color]\" class=\"my-color-field\" ><small>(For Metro Layout Only)</small>");

$sls_wp_mdo[] = array("field_name" => "style_results_font", "default" => "", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Results Font", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='style_results_font' value=\"$sls_wp_vars[style_results_font]\" class=\"my-color-field\" >");


// labels

$sls_wp_vars['cancel'];
$sls_wp_vars['no_brands_found'];
$sls_wp_vars['load_more'];


$sls_wp_mdo[] = array("field_name" => "of_label", "default" => "of", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("of", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='of_label' value=\"$sls_wp_vars[of_label]\" size='14'>");

$sls_wp_mdo[] = array("field_name" => "brands_label", "default" => "Brands & Partners", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Brands & Partners", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='brands_label' value=\"$sls_wp_vars[brands_label]\" size='14'>");


$sls_wp_mdo[] = array("field_name" => "results_label", "default" => "results", "input_zone" => "labels", "results_zone" => "sls_wp_dyn_js", "label" => __("results", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='results_label' value=\"$sls_wp_vars[results_label]\" size='14'>");

$sls_wp_mdo[] = array("field_name" => "show_all_label", "default" => "Show All", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Show All", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='show_all_label' value=\"$sls_wp_vars[show_all_label]\" size='14'>");


$sls_wp_mdo[] = array("field_name" => "all_category", "default" => "All", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("All", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='all_category' value=\"$sls_wp_vars[all_category]\" size='14'>");



$sls_wp_mdo[] = array("field_name" => "select_label", "default" => "Select", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Select", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='select_label' value=\"$sls_wp_vars[select_label]\" size='14'>");

$sls_wp_mdo[] = array("field_name" => "cancel", "default" => "Cancel", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Cancel", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='cancel' value=\"$sls_wp_vars[cancel]\" size='14'>");

$sls_wp_mdo[] = array("field_name" => "information_label", "default" => "Information", "input_zone" => "labels", "output_zone" => "information_label", "label" => __("Information", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='information_label' value=\"".$sls_wp_vars['information_label']."\" size='14'>", "stripslashes" => 1);

$sls_wp_mdo[] = array("field_name" => "features_label", "default" => "Features", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Features", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='features_label' value=\"".$sls_wp_vars['features_label']."\" size='14'>", "stripslashes" => 1);

/*$sls_wp_mdo[] = array("field_name" => "nearby_outlets_label", "default" => "Nearby Outlets", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Nearby Outlets", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='nearby_outlets_label' value=\"".$sls_wp_vars['nearby_outlets_label']."\" size='14'>", "stripslashes" => 1);*/

$sls_wp_mdo[] = array("field_name" => "additional_info_label", "default" => "Additional Info", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Additional Info", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='additional_info_label' value=\"".$sls_wp_vars['additional_info_label']."\" size='14'>");

$sls_wp_mdo[] = array("field_name" => "location_label", "default" => "Location", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Location", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='location_label' value=\"".$sls_wp_vars['location_label']."\" size='14'>");

$sls_wp_mdo[] = array("field_name" => "contact_details_label", "default" => "Contact Details", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Contact Details", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='contact_details_label' value=\"".$sls_wp_vars['contact_details_label']."\" size='14'>");

$sls_wp_mdo[] = array("field_name" => "streetview_label", "default" => "Categories", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js",  "label" => __("Categories", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='streetview_label' value=\"".$sls_wp_vars['streetview_label']."\" size='14'>");


$sls_wp_mdo[] = array("field_name" => "no_brands_found", "default" => "No brands found.", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("No brands found", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='no_brands_found' value=\"$sls_wp_vars[no_brands_found]\" size='14'>");

$sls_wp_mdo[] = array("field_name" => "load_more", "default" => "Load More", "input_zone" => "labels", "output_zone" => "sls_wp_dyn_js", "label" => __("Load More", SLS_WP_TEXT_DOMAIN), "input_template" => "<input type='text' name='load_more' value=\"$sls_wp_vars[load_more]\" size='14'>");




?>