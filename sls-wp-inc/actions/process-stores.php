<?php

	if (!empty($_GET['delete'])) {

		if (!empty($_GET['_wpnonce']) && wp_verify_nonce($_GET['_wpnonce'], "delete-location_".$_GET['delete'])){
			$wpdb->query($wpdb->prepare("DELETE FROM ".SLS_WP_TABLE." WHERE sls_wp_id='%d'", $_GET['delete'])); 
			sls_wp_process_tags("", "delete", $_GET['delete']); 
		} 
	}
	
	if (!empty($_GET['approve']) && empty($_POST) && $_POST['act']!="delete") {
      $wpdb->query($wpdb->prepare("UPDATE ".SLS_WP_TABLE." SET sls_wp_status=1 WHERE sls_wp_id='%d'", $_GET['approve'])); 
		
	}
	
	if (!empty($_POST) && !empty($_GET['edit']) && $_POST['act']!="delete") {
		$field_value_str=""; 
		foreach ($_POST as $key=>$value) {
			if (preg_match("@\-$_GET[edit]@", $key)) {
				$key=str_replace("-$_GET[edit]", "", $key); 
				if ($key=="sls_wp_tags") {

					$value=sls_wp_prepare_tag_string($value);

				}
				
				if (is_array($value)){
					$value=serialize($value); 
					$field_value_str.=$key."='$value',";
				} else {
					$field_value_str.=$key."=".$wpdb->prepare("%s", trim(sls_comma(stripslashes($value)))).", "; 
				}
				$_POST["$key"]=$value; 
			}
		}
		
		$field_value_str=substr($field_value_str, 0, strlen($field_value_str)-2);
		$edit=$_GET['edit']; extract($_POST);
		$the_address="$sls_wp_address, $sls_wp_city, $sls_wp_state $sls_wp_zip";
		
		if (empty($_POST['no_geocode']) || $_POST['no_geocode']!=1) { 
			$old_address=$wpdb->get_results("SELECT * FROM ".SLS_WP_TABLE." WHERE sls_wp_id='".esc_sql($_GET['edit'])."'", ARRAY_A); 
		}
		
		
		 
		$wpdb->query($wpdb->prepare("UPDATE ".SLS_WP_TABLE." SET ".str_replace("%", "%%", $field_value_str)." WHERE sls_wp_id='%d'", $_GET['edit']));  
		
		if(!empty($_POST['sls_wp_tags'])){sls_wp_process_tags($_POST['sls_wp_tags'], "insert", $_GET['edit']);}
		
		if ((empty($_POST['sls_wp_longitude']) || $_POST['sls_wp_longitude']==$old_address[0]['sls_wp_longitude']) && (empty($_POST['sls_wp_latitude']) || $_POST['sls_wp_latitude']==$old_address[0]['sls_wp_latitude'])) {
			if ($the_address!=$old_address[0]['sls_wp_address']." ".$old_address[0]['sls_wp_address2'].", ".$old_address[0]['sls_wp_city'].", ".$old_address[0]['sls_wp_state']." ".$old_address[0]['sls_wp_zip'] || ($old_address[0]['sls_wp_latitude']==="" || $old_address[0]['sls_wp_longitude']==="")) {
				sls_wp_do_geocoding($the_address,$_GET['edit']);
			}
		}
		print "<script>location.replace('".str_replace("&edit=$_GET[edit]", "", $_SERVER['REQUEST_URI'])."');</script>";
	}
	
	if (!empty($_POST['act']) && !empty($_POST['sls_wp_id']) && $_POST['act']=="delete") {
		
		if (!empty($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], "manage-locations_bulk")){
			include(SLS_WP_ACTIONS_PATH."/delete-stores.php");
		} else {
			print "<div class='sls-wp-menu-alert'>Security check doesn't validate for bulk deletion of locations.</div>";
		}
	}
	if (!empty($_POST['act']) && !empty($_POST['sls_wp_id']) && preg_match("@tag@", $_POST['act'])) {
		
		include(SLS_WP_ACTIONS_PATH."/tag-stores.php");
	}
	if (!empty($_POST['act']) && ($_POST['act']=='add_multi' || $_POST['act']=='remove_multi')) {
		
		include(SLS_WP_ADDONS_PATH."/multiple-field-updater/multiLocationUpdate.php");
	}
	if (!empty($_POST['act']) && $_POST['act']=="locationsPerPage") {
		
		$sls_wp_vars['admin_locations_per_page']=$_POST['sls_wp_admin_locations_per_page'];
		sls_wp_data('sls_wp_vars', 'update', $sls_wp_vars);
		extract($_POST);
	}
	if (!empty($_POST['act']) && $_POST['act']=="regeocode" && file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/reGeo.php")) {
		include(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/reGeo.php");
	}
	if (!empty($_GET['changeView']) && $_GET['changeView']==1) {
		if ($sls_wp_vars['location_table_view']=="Normal") {
			$sls_wp_vars['location_table_view']='Expanded';
			sls_wp_data('sls_wp_vars', 'update', $sls_wp_vars);
			
		} else {
			$sls_wp_vars['location_table_view']='Normal';
			sls_wp_data('sls_wp_vars', 'update', $sls_wp_vars);
			
		}
		print "<script>location.replace('".str_replace("&changeView=1", "", $_SERVER['REQUEST_URI'])."');</script>";
	}
	if (!empty($_GET['changeUpdater']) && $_GET['changeUpdater']==1) {
		if (sls_wp_data('sls_wp_location_updater_type')=="Tagging") {
			sls_wp_data('sls_wp_location_updater_type', 'update', 'Multiple Fields');
			
		} else {
			sls_wp_data('sls_wp_location_updater_type', 'update', 'Tagging');
			
		}
		$_SERVER['REQUEST_URI']=str_replace("&changeUpdater=1", "", $_SERVER['REQUEST_URI']);
		print "<script>location.replace('$_SERVER[REQUEST_URI]');</script>";
	}
	
?>