<?php
/*
Plugin Name: Super Logos Showcase Wordpress
Plugin URI: http://www.superstorefinder.net
Description: Super Logos Showcase is a plugin to showcase logos and brands on your website that is tailor made for full-width website template and mobile responsive view.
Version: 2.1
Author: Joe Iz
Author URI: http://www.superstorefinder.net
*/

$sls_wp_version="2.1";
define('SLS_WP_VERSION', $sls_wp_version);
$sls_wp_db_version=4.0;
include_once("sls-wp-define.php");
include_once(SLS_WP_INCLUDES_PATH."/copyfolderlibrary.php");

add_action('admin_menu', 'sls_wp_add_options_page');
add_action('wp_head', 'sls_wp_head_scripts');


include_once("sls-wp-functions.php");


register_activation_hook( __FILE__, 'sls_wp_install_tables');

add_action('the_content', 'sls_wp_template');
	
if (preg_match("@$sls_wp_dir@", $_SERVER['REQUEST_URI'])) {
	add_action("admin_print_scripts", 'sls_wp_add_admin_javascript');
	add_action("admin_print_styles",'sls_wp_add_admin_stylesheet');
}
load_plugin_textdomain(SLS_WP_TEXT_DOMAIN, "", "../uploads/sls-wp-uploads/languages/");



function sls_wp_plugin_prevent_upgrade($opt) {
	global $update_class;
	$plugin = plugin_basename(__FILE__);
	if ( $opt && isset($opt->response[$plugin]) ) {

		$update_class="update-message";

	}
	return $opt;
}

function sls_wp_update_db_check() {
    global $sls_wp_db_version;
    if (sls_wp_data('sls_wp_db_version') != $sls_wp_db_version) {
        sls_wp_install_tables();
    }
}
add_action('plugins_loaded', 'sls_wp_update_db_check');

add_action('activated_plugin','sls_save_error');
function sls_save_error(){
    update_option('plugin_error',  ob_get_contents());
}
?>